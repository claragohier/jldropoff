<?php

namespace Tests\Entity;

use PHPUnit\Framework\TestCase;
use App\Entity\User;

class UserTest extends TestCase
{
    public function testGetUser(){
        $user = new User();
        $user->setEmail('clara.gohier@gmail.com');
        $user->setPassword('123456');
        $this->assertSame('clara.gohier@gmail.com', $user->getEmail());
        $this->assertSame('123456', $user->getPassword());
    }
}
