<?php

namespace App\Controller;

use App\Entity\Document;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class DocumentController extends AbstractController
{
    
    /**
     * allows to create a document type
     * 
     * @Route("/document", name="app_create_document", methods={"POST"})
     */
    public function create(Request $request, SerializerInterface $serializer){
        
        $data = $request->getContent();

        $document = $serializer->deserialize($data, Document::class, 'json');
        assert($document instanceof Document);

        $em = $this->getDoctrine()->getManager();
        $em->persist($document);
        $em->flush();

        return new Response('Document créé avec succès', Response::HTTP_CREATED, [], [
            ObjectNormalizer::GROUPS => ['document']
        ]);
    }

}
