<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class SecurityController extends AbstractController
{
    /**
     * comparison of login and password using a sql request, 
     * if they are the same as in the database, it's ok
     * 
     * @Route("/login", name="app_login", methods={"POST"})
     */
    public function login(Request $request, SerializerInterface $serializer)
    {
        $data = json_decode($request->getContent(), true);

        $username = $data['username'];
        $password = $data['password'];

        $sql = 'SELECT * FROM user WHERE email = :username';
        $user = $this->getDoctrine()->getConnection()->fetchAll($sql, compact('username'));

        $user_password = $user[0]['password'];

        $password_verify = password_verify($password, $user_password);

        if ($password_verify === true) {

            $sql_user = 'SELECT id, email, roles, lastname, firstname, validated_profile FROM user WHERE email = :username';
            $user_to_store = $this->getDoctrine()->getConnection()->fetchAll($sql_user, compact('username'));

            $user_ok = $serializer->serialize($user_to_store[0], 'json');

            return new Response($user_ok, Response::HTTP_OK);
        } else {

            return new Response('Informations invalides', Response::HTTP_BAD_REQUEST);
        }
    }
}
