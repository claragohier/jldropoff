<?php

namespace App\Controller;

use App\Entity\Affair;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class AffairController extends AbstractController
{

    /**
     * allows employees to create a affair, which creates a folder on the server
     * 
     * @Route("/affair", name="app_create_affair", methods={"POST"})
     */
    public function create(Request $request, SerializerInterface $serializer)
    {
        $data = $request->getContent();

        $affair = $serializer->deserialize($data, Affair::class, 'json');
        assert($affair instanceof Affair);

        $affairs_number = $affair->getAffairsNumber();

        $ftp = ftp_connect('ftp.santeassurance.eu');
        $connect_id = ftp_login($ftp, 'clara@stage.santeassurance.eu', 'Stage34500');

        $dir = 'Affaire_' . $affairs_number;

        if (ftp_mkdir($ftp, $dir)) {

            $affair->setStatus(0);

            $em = $this->getDoctrine()->getManager();
            $em->persist($affair);
            $em->flush();

            return new Response('Affaire créée avec succès', Response::HTTP_CREATED, [], [
                ObjectNormalizer::GROUPS => ['affair']
            ]);
        }

        return new Response("Le répertoire n'a pas pu être créé");
    }

    /**
     * allows to retrieve a user's affairs, 
     * store in a different array depending on the status
     * 
     * @Route("/affair/{id}", name="app_get_affair", methods={"GET"})
     */
    public function getAffair(int $id, SerializerInterface $serializer)
    {
        $sql = 'SELECT * FROM affair WHERE fk_user_id = :id AND status = 0';
        $affair_false = $this->getDoctrine()->getConnection()->fetchAll($sql, compact('id'));

        $sql = 'SELECT * FROM affair WHERE fk_user_id = :id AND status = 1';
        $affair_true = $this->getDoctrine()->getConnection()->fetchAll($sql, compact('id'));

        $affair = [$affair_false, $affair_true];

        $serialized_affair = $serializer->serialize($affair, 'json');

        return new Response($serialized_affair, Response::HTTP_OK);
    }
}
