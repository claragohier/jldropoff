<?php

namespace App\Controller;

use App\Entity\Email;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class EmailController extends AbstractController
{

    /**
     * allows to add a email
     * 
     * @Route("/email", name="app_create_email", methods={"POST"})
     */
    public function create(Request $request, SerializerInterface $serializer)
    {
        $data = $request->getContent();

        $email = $serializer->deserialize($data, Email::class, 'json', [
            ObjectNormalizer::GROUPS => ['email']
        ]);
        assert($email instanceof Email);

        $em = $this->getDoctrine()->getManager();
        $em->persist($email);
        $em->flush();

        return new Response('Email envoyé avec succès', Response::HTTP_CREATED, [], [
            ObjectNormalizer::GROUPS => ['email']
        ]);
    }

    /**
     * allows to retrieve a email
     * 
     * @Route("/email/{id}", name="app_get_email", methods={"POST"})
     */
    public function getEmail(Email $email)
    {
        return $this->json($email);
    }
}
