<?php

namespace App\Controller;

use App\Entity\EmailDescription;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class EmailDescriptionController extends AbstractController
{

    /**
     * allows to add a email description
     * 
     * @Route("/email/description", name="app_create_email_description", methods={"POST"})
     */
    public function create(Request $request, SerializerInterface $serializer){
        
        $data = $request->getContent();

        $email_description = $serializer->deserialize($data, EmailDescription::class, 'json');
        assert($email_description instanceof EmailDescription);

        $em = $this->getDoctrine()->getManager();
        $em->persist($email_description);
        $em->flush();

        return new Response('Description d\'email créé avec succès', Response::HTTP_CREATED);
    }

}
