<?php

namespace App\Controller;

use App\Entity\Origin;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class OriginController extends AbstractController
{
    
    /**
     * allows to add a document origin
     * 
     * @Route("/origin", name="app_create_origin", methods={"POST"})
     */
    public function create(Request $request, SerializerInterface $serializer)
    {
        $data = $request->getContent();

        $origin = $serializer->deserialize($data, Origin::class, 'json');
        assert($origin instanceof Origin);

        $em = $this->getDoctrine()->getManager();
        $em->persist($origin);
        $em->flush();

        return new Response('Origine créée avec succès', Response::HTTP_CREATED);
    }
}
