<?php

namespace App\Controller;

use App\Entity\Phone;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class PhoneController extends AbstractController
{
    /**
     * allows to add a phone number
     * 
     * @Route("/phone", name="app_create_phone", methods={"POST"})
     */
    public function create(Request $request, SerializerInterface $serializer)
    {
        $data = $request->getContent();
       
        $phone = $serializer->deserialize($data, Phone::class, 'json', [
            ObjectNormalizer::GROUPS => ['phone']
        ]); 
        assert($phone instanceof Phone);
       
        $em = $this->getDoctrine()->getManager();
        $em->persist($phone);
        $em->flush();

        return new Response('Numéro de téléphone envoyé avec succès', Response::HTTP_CREATED, [], [
            ObjectNormalizer::GROUPS => ['phone']
        ]);
    }

    /**
     * allows to retrieve a phone number
     * 
     * @Route("/phone/{id}", name="app_get_phone", methods={"GET"})
     */
    public function getPhone(Phone $phone)
    {
        return $this->json($phone);
    }
}
