<?php

namespace App\Controller;

use App\Entity\PhoneDescription;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class PhoneDescriptionController extends AbstractController
{
    /**
     * allows to add a phone description
     * 
     * @Route("/phone/description", name="app_create_phone_description", methods={"POST"})
     */
    public function create(Request $request, SerializerInterface $serializer){
        
        $data = $request->getContent();

        $phone_description = $serializer->deserialize($data, PhoneDescription::class, 'json');
        assert($phone_description instanceof PhoneDescription);

        $em = $this->getDoctrine()->getManager();
        $em->persist($phone_description);
        $em->flush();

        return new Response('Description de téléphone créé avec succès', Response::HTTP_CREATED);
    }
}
