<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RegistrationController extends AbstractController
{

    /**
     * allows employees to create a new customer
     * 
     * @Route("/register", name="app_register", methods={"POST"})
     */
    public function registration(Request $request, SerializerInterface $serializer, ValidatorInterface $validator)
    {
        $data = $request->getContent();

        $user = $serializer->deserialize($data, User::class, 'json');
        assert($user instanceof User);

        $errors = $validator->validate($user);

        if (count($errors) > 0) {

            $errorsString = (string) $errors;

            return new Response($errorsString);
        }

        $user->setRoles(['ROLE_USER']);
        $user->setValidatedProfile(0);

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return new Response('Utilisateur créé', Response::HTTP_CREATED);
    }

    /**
     * allows the user to update their registration by adding a password 
     * (if employees have already created it)
     * sending a confirmation email
     * 
     * @Route("/user_register", name="app_user_register", methods={"PUT"})
     */
    public function userRegister(Request $request, SerializerInterface $serializer, UserRepository $userRepository, UserPasswordEncoderInterface $encoder, \Swift_Mailer $mailer)
    {
        $data = $request->getContent();
        $user = $serializer->deserialize($data, User::class, 'json');
        assert($user instanceof User);

        $email = $user->getEmail();

        $user_to_update = $userRepository->findOneBy(['email' => $email]);

        if ($user_to_update != null) {

            $user = $serializer->deserialize(
                $data,
                User::class,
                'json',
                [AbstractNormalizer::OBJECT_TO_POPULATE => $user_to_update]
            );

            $password = $user->getPassword();
            $user->setPlainPassword($password);

            $encoded_password = $encoder->encodePassword($user, $password);
            $user->setPassword($encoded_password);

            $user->setValidatedProfile(0);
            $user->setActivationToken(md5(uniqid()));

            // call method of sending email
            $this->sendEmail($user, $mailer);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return new Response('Utilisateur bien mis à jour', Response::HTTP_OK);
        }

        return new Response('Cet email n\'est pas enregistré', Response::HTTP_BAD_REQUEST);
    }

    /**
     * sending mail using Swiftmailer
     */
    public function sendEmail($user, \Swift_Mailer $mailer)
    {
        $message = (new \Swift_Message('Nouveau compte'))
            ->setFrom('christophe@australdev.eu')
            ->setTo('clara.gohier@gmail.com')
            ->setBody(
                $this->renderView(
                    'emails/activation.html.twig',
                    ['token' => $user->getActivationToken()]
                ),
                'text/html'
            );
        $mailer->send($message);
    }

    /**
     * account activation if the email has been clicked
     * 
     * @Route("/activation/{token}", name="activation")
     */
    public function activation($token, UserRepository $userRepository)
    {
        $user = $userRepository->findOneBy(['activation_token' => $token]);

        if (!$user) {
            return new Response('Cet utilisateur n\'existe pas');
        }

        $user->setActivationToken(null);
        $user->setValidatedProfile(1);
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return new Response('Utilisateur activé avec succès');
    }
}
