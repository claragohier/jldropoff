<?php

namespace App\Controller;

use App\Entity\Upload;
use App\Repository\UploadRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

class UploadController extends AbstractController
{

    /**
     * allows employees to request a file, 
     * contains the condition that determines if the affair is over
     * 
     * @Route("/upload/file", name="app_upload_file", methods={"POST"})
     */
    public function createFile(Request $request, SerializerInterface $serializer)
    {
        $data = $request->getContent();

        $file = $serializer->deserialize($data, Upload::class, 'json');
        $file->setUpload(false);

        $document_type = $file->getFkDocument()->getLabel();
        $document_origin = $file->getFkDocument()->getFkOrigin()->getLabel();

        if ($document_type === 'Contrat' && $document_origin === 'JL Assure') {
            $file->getFkAffair()->setStatus(1);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($file);
        $em->flush();

        return new Response("Fichier bien créé");
    }

    /**
     * allows the user to send the files that are requested,
     * sending this file to the server,
     * and sending useful data to the database,
     * sending emails to employees to announce the file has been sent
     * 
     * @Route("/uploaded/file", name="app_uploaded_file", methods={"POST"})
     */
    public function create(Request $request, SerializerInterface $serializer, UploadRepository $uploadRepository,  \Swift_Mailer $mailer)
    {
        // connection to the ftp server
        $ftp = ftp_connect('ftp.santeassurance.eu');
        $connected = ftp_login($ftp, 'clara@stage.santeassurance.eu', 'Stage34500');

        // retrieving the data contained in the request
        $img = $request->files->get('file');

        $extension = pathinfo($img->getClientOriginalName(), PATHINFO_EXTENSION);

        // retrieving the content of the request
        $data = $request->get('content');

        // data deserialization
        $content = $serializer->deserialize(
            $data,
            Upload::class,
            'json'
        );

        // foreign key recovery
        $document = $content->getFkDocument();
        $affair = $content->getFkAffair();

        // email preparation
        $origin = $document->getFkOrigin()->getLabel();
        $user_firstname = $affair->getFkUser()->getFirstname();
        $user_lastname = $affair->getFkUser()->getLastname();

        // storage of the current affair id as well as the type of file sent
        $affair_id = $affair->getId();
        $affairs_number = $affair->getAffairsNumber();
        $document_id = $document->getId();
        $file_type = $document->getLabelServer();

        $sql = 'SELECT * FROM upload WHERE fk_affair_id = :affair_id AND fk_document_id = :document_id';
        $file = $this->getDoctrine()->getConnection()->fetchAll($sql, compact('affair_id', 'document_id'));

        // retrieve the id of the file to update
        $file_id = $file[0]['id'];
        $file_id = $serializer->deserialize($file_id, Upload::class, 'json');
        $upload_id = $file_id->getId();

        $datetime = new DateTime('now');
        $timestamp = $datetime->getTimestamp();

        // store the directory route and the desired file name
        $directory = "/Affaire_" . $affairs_number;
        $filename = $affairs_number . '-' . $file_type . '-' . $timestamp . '.' . $extension;

        // add the file to the directory
        if (ftp_put($ftp, $directory . '/' . $filename, $img, FTP_BINARY)) {

            // retrieving the file path to send to the database
            $file_path = $directory . '/' . $filename;

            // instantiating a new uploaded_file object
            $uploaded_file = $uploadRepository->find($upload_id);

            // set of properties
            $uploaded_file->setCreatedDate($datetime);
            $uploaded_file->setFilePath($file_path);
            $uploaded_file->setUpload(true);
            $uploaded_file->setFkDocument($document);
            $uploaded_file->setFkAffair($affair);

            // sending the object to the database
            $em = $this->getDoctrine()->getManager();
            $em->persist($uploaded_file);
            $em->flush();

            // sending an email to each document added by a client
            if ($origin === 'Client') {
                $this->sendEmail($affairs_number, $user_firstname, $user_lastname, $mailer);
            }

            ftp_close($ftp);
            return new Response("Fichier bien ajouté", Response::HTTP_OK);
        }

        ftp_close($ftp);
        return new Response("Le fichier n'a pas été envoyé");
    }


    /**
     * allows file update
     * 
     * @Route("/upload/{id}", name="app_update_uploaded_file", methods={"POST"})
     */
    public function updateFile($id, Upload $upload, Request $request, SerializerInterface $serializer)
    {
        $ftp = ftp_connect('ftp.santeassurance.eu');
        $connected = ftp_login($ftp, 'clara@stage.santeassurance.eu', 'Stage34500');

        $img = $request->files->get('file');

        $sql = 'SELECT file_path FROM upload WHERE id = :id';
        $file_path = $this->getDoctrine()->getConnection()->fetchAll($sql, compact('id'));

        $filename = $file_path[0]['file_path'];

        if (ftp_put($ftp, $filename, $img, FTP_BINARY)) {
            return new Response("Fichier bien mis à jour", Response::HTTP_OK);
        }

        ftp_close($ftp);
        return new Response("Le fichier n'a pas été envoyé");
    }

    /**
     * to recover the missing files, thanks to an sql request
     * 
     * @Route("/missing_files/{affair_id}", name="app_get_missing_files", methods={"GET"})
     */
    public function getMissingFiles($affair_id, SerializerInterface $serializer)
    {
        $sql = 'SELECT up.fk_document_id, doc.label FROM upload up, document doc, origin ori WHERE upload = false 
            AND up.fk_affair_id = :affair_id 
            AND doc.id = up.fk_document_id
            AND doc.fk_origin_id = ori.id
            AND ori.label = "Client"';
        $missing_files = $this->getDoctrine()->getConnection()->fetchAll($sql, compact('affair_id'));

        $missing_files = $serializer->serialize($missing_files, 'json');
        return new Response($missing_files, Response::HTTP_OK);
    }

    /**
     * to recover the uploaded files, thanks to an sql request
     * 
     * @Route("uploaded_files/{affair_id}", name="app_get_uploaded_files", methods={"GET"})
     */
    public function getUploadedFiles($affair_id, SerializerInterface $serializer)
    {
        $sql = 'SELECT up.file_path, doc.label FROM upload up, document doc, origin ori WHERE up.fk_affair_id = :affair_id 
            AND up.fk_document_id = doc.id 
            AND doc.fk_origin_id = ori.id 
            AND ori.label = "Client"
            AND up.upload = true';
        $uploaded_files = $this->getDoctrine()->getConnection()->fetchAll($sql, compact('affair_id'));

        $uploaded_files = $serializer->serialize($uploaded_files, 'json');
        return new Response($uploaded_files, Response::HTTP_OK);
    }

    /**
     * to recover the received files, thanks to an sql request
     * 
     * @Route("received_files/{affair_id}", name="app_get_received_files", methods={"GET"})
     */
    public function getReceivedFiles($affair_id, SerializerInterface $serializer)
    {
        $sql = 'SELECT up.file_path, doc.label FROM upload up, document doc, origin ori WHERE up.fk_affair_id = :affair_id 
            AND up.fk_document_id = doc.id 
            AND doc.fk_origin_id = ori.id 
            AND ori.label = "JL Assure"
            AND up.upload = true';
        $received_files = $this->getDoctrine()->getConnection()->fetchAll($sql, compact('affair_id'));

        $received_files = $serializer->serialize($received_files, 'json');
        return new Response($received_files, Response::HTTP_OK);
    }

    /**
     * sending mail using Swiftmailer
     */
    public function sendEmail($affairs_number, $user_firstname, $user_lastname, \Swift_Mailer $mailer)
    {
        $message = (new \Swift_Message('Document téléversé'))
            ->setFrom('christophe@australdev.eu')
            ->setTo('clara.gohier@gmail.com')
            ->setBody(
                $this->renderView(
                    'emails/confirmed.html.twig',
                    [
                        'affairs_number' => $affairs_number,
                        'userFirstname' => $user_firstname,
                        'userLastname' => $user_lastname
                    ],
                ),
                'text/html'
            )
            ->setCc('jyllozaki@gmail.com');
        $mailer->send($message);
    }

    /**
     * delete a given file in database and server, reset affair status
     * 
     * @Route("/file/{directory}/{filename}/{extension}", name="app_delete_fileFTP", methods={"DELETE"})
     */
    public function deleteFileFTP($directory, $filename, $extension, UploadRepository $uploadRepository)
    {
        $ftp = ftp_connect('ftp.santeassurance.eu');
        $connected = ftp_login($ftp, 'clara@stage.santeassurance.eu', 'Stage34500');

        $filepath = $directory . '/' . $filename . '.' . $extension;

        if (ftp_delete($ftp, $filepath)) {

            $file = $uploadRepository->findOneBy(['file_path' => '/' . $filepath]);

            $file->getFkAffair()->setStatus(0);

            $em = $this->getDoctrine()->getManager();
            $em->remove($file);
            $em->flush();

            return new Response("Fichier supprimé");
        } else {
            return new Response("Le fichier n'a pas été supprimé");
        }
    }

    /**
     * @Route("/uploaded/{affair}/{file_path}/{extension}", name="app_get_upload_file", methods={"GET"})
     */
    public function getFile($affair, $file_path, $extension, UploadRepository $uploadRepository, SerializerInterface $serializer)
    {
        $new_file_path = '/' . $affair . '/' . $file_path . '.' . $extension;
        $file = $uploadRepository->findOneBy(['file_path' => $new_file_path]);

        $file = $serializer->serialize($file, 'json', [
            ObjectNormalizer::GROUPS => ['file', 'affair', 'document']
        ]);

        return new Response($file);
    }
}
