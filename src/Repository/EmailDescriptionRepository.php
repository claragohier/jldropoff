<?php

namespace App\Repository;

use App\Entity\EmailDescription;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EmailDescription|null find($id, $lockMode = null, $lockVersion = null)
 * @method EmailDescription|null findOneBy(array $criteria, array $orderBy = null)
 * @method EmailDescription[]    findAll()
 * @method EmailDescription[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmailDescriptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EmailDescription::class);
    }

    // /**
    //  * @return EmailDescription[] Returns an array of EmailDescription objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EmailDescription
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
