<?php

namespace App\Repository;

use App\Entity\PhoneDescription;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PhoneDescription|null find($id, $lockMode = null, $lockVersion = null)
 * @method PhoneDescription|null findOneBy(array $criteria, array $orderBy = null)
 * @method PhoneDescription[]    findAll()
 * @method PhoneDescription[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PhoneDescriptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PhoneDescription::class);
    }

    // /**
    //  * @return PhoneDescription[] Returns an array of PhoneDescription objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PhoneDescription
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
