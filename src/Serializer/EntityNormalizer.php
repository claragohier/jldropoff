<?php

declare(strict_types=1);

namespace App\Serializer;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;

class EntityNormalizer  implements ContextAwareDenormalizerInterface
{
    private EntityManagerInterface $em;
    /**
     * EntityNormalizer constructor.
     * @param EntityManagerInterface $em
     * @param ClassMetadataFactoryInterface $classMetadataFactory
     */
    public function __construct(EntityManagerInterface $em, ClassMetadataFactoryInterface $classMetadataFactory)
    {
        $this->em = $em;
        $this->classMetadataFactory = $classMetadataFactory;
    }
    public function supportsDenormalization($data, string $type, string $format = null, array $context = []): bool
    {
        return is_numeric($data) && 0 === strpos($type, 'App\\Entity\\');
    }
    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        return $this->em->getReference($type, $data);
    }
}
