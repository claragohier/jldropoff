<?php

namespace App\Entity;

use App\Repository\EmailDescriptionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=EmailDescriptionRepository::class)
 */
class EmailDescription
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"email"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $label;

    /**
     * @ORM\OneToMany(targetEntity=Email::class, mappedBy="fk_emailDescription", orphanRemoval=true)
     */
    private $fk_email;

    public function __construct()
    {
        $this->fk_email = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection|Email[]
     */
    public function getFkEmail(): Collection
    {
        return $this->fk_email;
    }

    public function addFkEmail(Email $fkEmail): self
    {
        if (!$this->fk_email->contains($fkEmail)) {
            $this->fk_email[] = $fkEmail;
            $fkEmail->setFkEmailDescription($this);
        }

        return $this;
    }

    public function removeFkEmail(Email $fkEmail): self
    {
        if ($this->fk_email->removeElement($fkEmail)) {
            // set the owning side to null (unless already changed)
            if ($fkEmail->getFkEmailDescription() === $this) {
                $fkEmail->setFkEmailDescription(null);
            }
        }

        return $this;
    }
}
