<?php

namespace App\Entity;

use App\Repository\AffairRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=AffairRepository::class)
 */
class Affair
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"affair", "file"})
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"affair"})
     */
    private $affairs_number;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({"affair"})
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"affair"})
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="fk_affair", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"affair"})
     */
    private $fk_user;

    /**
     * @ORM\OneToMany(targetEntity=Upload::class, mappedBy="fk_affair", orphanRemoval=true)
     */
    private $fk_upload;

    /**
     * @ORM\Column(type="boolean")
     */
    private $status;

    public function __construct()
    {
        $this->upload = new ArrayCollection();
        $this->fk_upload = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAffairsNumber(): ?int
    {
        return $this->affairs_number;
    }

    public function setAffairsNumber(int $affairs_number): self
    {
        $this->affairs_number = $affairs_number;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getFkUser(): ?User
    {
        return $this->fk_user;
    }

    public function setFkUser(?User $fk_user): self
    {
        $this->fk_user = $fk_user;

        return $this;
    }

    /**
     * @return Collection|Upload[]
     */
    public function getFkUpload(): Collection
    {
        return $this->fk_upload;
    }

    public function addFkUpload(Upload $fkUpload): self
    {
        if (!$this->fk_upload->contains($fkUpload)) {
            $this->fk_upload[] = $fkUpload;
            $fkUpload->setFkAffair($this);
        }

        return $this;
    }

    public function removeFkUpload(Upload $fkUpload): self
    {
        if ($this->fk_upload->removeElement($fkUpload)) {
            // set the owning side to null (unless already changed)
            if ($fkUpload->getFkAffair() === $this) {
                $fkUpload->setFkAffair(null);
            }
        }

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

}
