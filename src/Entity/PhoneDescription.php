<?php

namespace App\Entity;

use App\Repository\PhoneDescriptionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=PhoneDescriptionRepository::class)
 */
class PhoneDescription
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"phone"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({"phone"})
     */
    private $label;

    /**
     * @ORM\OneToMany(targetEntity=Phone::class, mappedBy="fk_phoneDescription", orphanRemoval=true)
     */
    private $fk_phone;

    public function __construct()
    {
        $this->fk_phone = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection|Phone[]
     */
    public function getFkPhone(): Collection
    {
        return $this->fk_phone;
    }

    public function addFkPhone(Phone $fkPhone): self
    {
        if (!$this->fk_phone->contains($fkPhone)) {
            $this->fk_phone[] = $fkPhone;
            $fkPhone->setFkPhoneDescription($this);
        }

        return $this;
    }

    public function removeFkPhone(Phone $fkPhone): self
    {
        if ($this->fk_phone->removeElement($fkPhone)) {
            // set the owning side to null (unless already changed)
            if ($fkPhone->getFkPhoneDescription() === $this) {
                $fkPhone->setFkPhoneDescription(null);
            }
        }

        return $this;
    }

}
