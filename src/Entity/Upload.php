<?php

namespace App\Entity;

use App\Repository\UploadRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=UploadRepository::class)
 */
class Upload
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"file"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"file"})
     */
    private $file_path;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"file"})
     */
    private $created_date;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"file"})
     */
    private $upload;

    /**
     * @ORM\ManyToOne(targetEntity=Affair::class, inversedBy="fk_upload")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"file"})
     */
    private $fk_affair;

    /**
     * @ORM\ManyToOne(targetEntity=Document::class, inversedBy="fk_upload")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"file"})
     */
    private $fk_document;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFilePath(): ?string
    {
        return $this->file_path;
    }

    public function setFilePath(string $file_path): self
    {
        $this->file_path = $file_path;

        return $this;
    }

    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->created_date;
    }

    public function setCreatedDate(\DateTimeInterface $created_date): self
    {
        $this->created_date = $created_date;

        return $this;
    }

    public function getUpload(): ?bool
    {
        return $this->upload;
    }

    public function setUpload(bool $upload): self
    {
        $this->upload = $upload;

        return $this;
    }

    public function getFkAffair(): ?Affair
    {
        return $this->fk_affair;
    }

    public function setFkAffair(?Affair $fk_affair): self
    {
        $this->fk_affair = $fk_affair;

        return $this;
    }

    public function getFkDocument(): ?Document
    {
        return $this->fk_document;
    }

    public function setFkDocument(?Document $fk_document): self
    {
        $this->fk_document = $fk_document;

        return $this;
    }

}
