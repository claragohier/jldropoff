<?php

namespace App\Entity;

use App\Repository\DocumentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=DocumentRepository::class)
 */
class Document
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"document", "file"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({"document"})
     */
    private $label;

    /**
     * @ORM\ManyToOne(targetEntity=Origin::class, inversedBy="fk_document")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"document"})
     */
    private $fk_origin;

    /**
     * @ORM\OneToMany(targetEntity=Upload::class, mappedBy="fk_document", orphanRemoval=true)
     */
    private $fk_upload;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $label_server;

    public function __construct()
    {
        $this->fk_upload = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getFkOrigin(): ?Origin
    {
        return $this->fk_origin;
    }

    public function setFkOrigin(?Origin $fk_origin): self
    {
        $this->fk_origin = $fk_origin;

        return $this;
    }

    /**
     * @return Collection|Upload[]
     */
    public function getFkUpload(): Collection
    {
        return $this->fk_upload;
    }

    public function addFkUpload(Upload $fkUpload): self
    {
        if (!$this->fk_upload->contains($fkUpload)) {
            $this->fk_upload[] = $fkUpload;
            $fkUpload->setFkDocument($this);
        }

        return $this;
    }

    public function removeFkUpload(Upload $fkUpload): self
    {
        if ($this->fk_upload->removeElement($fkUpload)) {
            // set the owning side to null (unless already changed)
            if ($fkUpload->getFkDocument() === $this) {
                $fkUpload->setFkDocument(null);
            }
        }

        return $this;
    }

    public function getLabelServer(): ?string
    {
        return $this->label_server;
    }

    public function setLabelServer(string $label_server): self
    {
        $this->label_server = $label_server;

        return $this;
    }

}
