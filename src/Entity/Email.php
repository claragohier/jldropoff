<?php

namespace App\Entity;

use App\Repository\EmailRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=EmailRepository::class)
 */
class Email
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"email"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"email"})
     */
    private $email;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="fk_email")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"email"})
     */
    private $fk_user;

    /**
     * @ORM\ManyToOne(targetEntity=EmailDescription::class, inversedBy="fk_email")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"email"})
     */
    private $fk_emailDescription;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getFkUser(): ?User
    {
        return $this->fk_user;
    }

    public function setFkUser(?User $fk_user): self
    {
        $this->fk_user = $fk_user;

        return $this;
    }

    public function getFkEmailDescription(): ?EmailDescription
    {
        return $this->fk_emailDescription;
    }

    public function setFkEmailDescription(?EmailDescription $fk_emailDescription): self
    {
        $this->fk_emailDescription = $fk_emailDescription;

        return $this;
    }

}
