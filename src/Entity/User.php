<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"affair","phone","email", "file"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\Email(message="The email {{ value }} is not a valid email.")
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string", nullable = true)
     */
    private $password;

    /**
    * @Assert\Length(min=6)
    */
    private $plainPassword;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $firstname;

    /**
     * @ORM\Column(type="boolean")
     */
    private $validated_profile;

    /**
     * @ORM\OneToMany(targetEntity=Affair::class, mappedBy="fk_user", orphanRemoval=true)
     */
    private $fk_affair;

    /**
     * @ORM\OneToMany(targetEntity=Email::class, mappedBy="fk_user", orphanRemoval=true)
     */
    private $fk_email;

    /**
     * @ORM\OneToMany(targetEntity=Phone::class, mappedBy="fk_user")
     */
    private $fk_phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $activation_token;

    public function __construct()
    {
        $this->fk_affair = new ArrayCollection();
        $this->fk_email = new ArrayCollection();
        $this->fk_phone = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getValidatedProfile(): ?bool
    {
        return $this->validated_profile;
    }

    public function setValidatedProfile(bool $validated_profile): self
    {
        $this->validated_profile = $validated_profile;

        return $this;
    }

    /**
     * @return Collection|Affair[]
     */
    public function getFkAffair(): Collection
    {
        return $this->fk_affair;
    }

    public function addFkAffair(Affair $fkAffair): self
    {
        if (!$this->fk_affair->contains($fkAffair)) {
            $this->fk_affair[] = $fkAffair;
            $fkAffair->setFkUser($this);
        }

        return $this;
    }

    public function removeFkAffair(Affair $fkAffair): self
    {
        if ($this->fk_affair->removeElement($fkAffair)) {
            // set the owning side to null (unless already changed)
            if ($fkAffair->getFkUser() === $this) {
                $fkAffair->setFkUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Email[]
     */
    public function getFkEmail(): Collection
    {
        return $this->fk_email;
    }

    public function addFkEmail(Email $fkEmail): self
    {
        if (!$this->fk_email->contains($fkEmail)) {
            $this->fk_email[] = $fkEmail;
            $fkEmail->setFkUser($this);
        }

        return $this;
    }

    public function removeFkEmail(Email $fkEmail): self
    {
        if ($this->fk_email->removeElement($fkEmail)) {
            // set the owning side to null (unless already changed)
            if ($fkEmail->getFkUser() === $this) {
                $fkEmail->setFkUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Phone[]
     */
    public function getFkPhone(): Collection
    {
        return $this->fk_phone;
    }

    public function addFkPhone(Phone $fkPhone): self
    {
        if (!$this->fk_phone->contains($fkPhone)) {
            $this->fk_phone[] = $fkPhone;
            $fkPhone->setFkUser($this);
        }

        return $this;
    }

    public function removeFkPhone(Phone $fkPhone): self
    {
        if ($this->fk_phone->removeElement($fkPhone)) {
            // set the owning side to null (unless already changed)
            if ($fkPhone->getFkUser() === $this) {
                $fkPhone->setFkUser(null);
            }
        }

        return $this;
    }

    public function getActivationToken(): ?string
    {
        return $this->activation_token;
    }

    public function setActivationToken(?string $activation_token): self
    {
        $this->activation_token = $activation_token;

        return $this;
    }
}
