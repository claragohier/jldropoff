<?php

namespace App\Entity;

use App\Repository\OriginRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=OriginRepository::class)
 */
class Origin
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"document", "file"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $label;

    /**
     * @ORM\OneToMany(targetEntity=Document::class, mappedBy="fk_origin", orphanRemoval=true)
     */
    private $fk_document;

    public function __construct()
    {
        $this->fk_document = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection|Document[]
     */
    public function getFkDocument(): Collection
    {
        return $this->fk_document;
    }

    public function addFkDocument(Document $fkDocument): self
    {
        if (!$this->fk_document->contains($fkDocument)) {
            $this->fk_document[] = $fkDocument;
            $fkDocument->setFkOrigin($this);
        }

        return $this;
    }

    public function removeFkDocument(Document $fkDocument): self
    {
        if ($this->fk_document->removeElement($fkDocument)) {
            // set the owning side to null (unless already changed)
            if ($fkDocument->getFkOrigin() === $this) {
                $fkDocument->setFkOrigin(null);
            }
        }

        return $this;
    }
}
