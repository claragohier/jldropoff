<?php

namespace App\Entity;

use App\Repository\PhoneRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=PhoneRepository::class)
 */
class Phone
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"phone"})
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"phone"})
     */
    private $phone_number;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="fk_phone")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"phone"})
     */
    private $fk_user;

    /**
     * @ORM\ManyToOne(targetEntity=PhoneDescription::class, inversedBy="fk_phone")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"phone"})
     */
    private $fk_phoneDescription;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPhoneNumber(): ?int
    {
        return $this->phone_number;
    }

    public function setPhoneNumber(int $phone_number): self
    {
        $this->phone_number = $phone_number;

        return $this;
    }

    public function getFkUser(): ?User
    {
        return $this->fk_user;
    }

    public function setFkUser(?User $fk_user): self
    {
        $this->fk_user = $fk_user;

        return $this;
    }

    public function getFkPhoneDescription(): ?PhoneDescription
    {
        return $this->fk_phoneDescription;
    }

    public function setFkPhoneDescription(?PhoneDescription $fk_phoneDescription): self
    {
        $this->fk_phoneDescription = $fk_phoneDescription;

        return $this;
    }

}
