<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210127080506 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE affair DROP FOREIGN KEY FK_397436879D86650F');
        $this->addSql('DROP INDEX IDX_397436879D86650F ON affair');
        $this->addSql('ALTER TABLE affair CHANGE user_id_id fk_user_id INT NOT NULL');
        $this->addSql('ALTER TABLE affair ADD CONSTRAINT FK_397436875741EEB9 FOREIGN KEY (fk_user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_397436875741EEB9 ON affair (fk_user_id)');
        $this->addSql('ALTER TABLE document DROP FOREIGN KEY FK_D8698A76C23E42B3');
        $this->addSql('DROP INDEX IDX_D8698A76C23E42B3 ON document');
        $this->addSql('ALTER TABLE document CHANGE origin_id_id fk_origin_id INT NOT NULL');
        $this->addSql('ALTER TABLE document ADD CONSTRAINT FK_D8698A7637FBA45C FOREIGN KEY (fk_origin_id) REFERENCES origin (id)');
        $this->addSql('CREATE INDEX IDX_D8698A7637FBA45C ON document (fk_origin_id)');
        $this->addSql('ALTER TABLE email DROP FOREIGN KEY FK_E7927C7430635D72');
        $this->addSql('ALTER TABLE email DROP FOREIGN KEY FK_E7927C749D86650F');
        $this->addSql('DROP INDEX IDX_E7927C7430635D72 ON email');
        $this->addSql('DROP INDEX IDX_E7927C749D86650F ON email');
        $this->addSql('ALTER TABLE email ADD fk_user_id INT NOT NULL, ADD fk_email_description_id INT NOT NULL, DROP user_id_id, DROP email_description_id_id');
        $this->addSql('ALTER TABLE email ADD CONSTRAINT FK_E7927C745741EEB9 FOREIGN KEY (fk_user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE email ADD CONSTRAINT FK_E7927C74730AB8CE FOREIGN KEY (fk_email_description_id) REFERENCES email_description (id)');
        $this->addSql('CREATE INDEX IDX_E7927C745741EEB9 ON email (fk_user_id)');
        $this->addSql('CREATE INDEX IDX_E7927C74730AB8CE ON email (fk_email_description_id)');
        $this->addSql('ALTER TABLE phone DROP FOREIGN KEY FK_444F97DD9D86650F');
        $this->addSql('ALTER TABLE phone DROP FOREIGN KEY FK_444F97DDD7F5047');
        $this->addSql('DROP INDEX IDX_444F97DDD7F5047 ON phone');
        $this->addSql('DROP INDEX IDX_444F97DD9D86650F ON phone');
        $this->addSql('ALTER TABLE phone ADD fk_user_id INT NOT NULL, ADD fk_phone_description_id INT NOT NULL, DROP user_id_id, DROP phone_description_id_id');
        $this->addSql('ALTER TABLE phone ADD CONSTRAINT FK_444F97DD5741EEB9 FOREIGN KEY (fk_user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE phone ADD CONSTRAINT FK_444F97DD62082E52 FOREIGN KEY (fk_phone_description_id) REFERENCES phone_description (id)');
        $this->addSql('CREATE INDEX IDX_444F97DD5741EEB9 ON phone (fk_user_id)');
        $this->addSql('CREATE INDEX IDX_444F97DD62082E52 ON phone (fk_phone_description_id)');
        $this->addSql('ALTER TABLE upload DROP FOREIGN KEY FK_17BDE61F16E5E825');
        $this->addSql('ALTER TABLE upload DROP FOREIGN KEY FK_17BDE61F174FF259');
        $this->addSql('DROP INDEX IDX_17BDE61F16E5E825 ON upload');
        $this->addSql('DROP INDEX IDX_17BDE61F174FF259 ON upload');
        $this->addSql('ALTER TABLE upload ADD fk_affair_id INT NOT NULL, ADD fk_document_id INT NOT NULL, DROP affair_id_id, DROP document_id_id');
        $this->addSql('ALTER TABLE upload ADD CONSTRAINT FK_17BDE61F3D163BCC FOREIGN KEY (fk_affair_id) REFERENCES affair (id)');
        $this->addSql('ALTER TABLE upload ADD CONSTRAINT FK_17BDE61FAAC9D403 FOREIGN KEY (fk_document_id) REFERENCES document (id)');
        $this->addSql('CREATE INDEX IDX_17BDE61F3D163BCC ON upload (fk_affair_id)');
        $this->addSql('CREATE INDEX IDX_17BDE61FAAC9D403 ON upload (fk_document_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE affair DROP FOREIGN KEY FK_397436875741EEB9');
        $this->addSql('DROP INDEX IDX_397436875741EEB9 ON affair');
        $this->addSql('ALTER TABLE affair CHANGE fk_user_id user_id_id INT NOT NULL');
        $this->addSql('ALTER TABLE affair ADD CONSTRAINT FK_397436879D86650F FOREIGN KEY (user_id_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_397436879D86650F ON affair (user_id_id)');
        $this->addSql('ALTER TABLE document DROP FOREIGN KEY FK_D8698A7637FBA45C');
        $this->addSql('DROP INDEX IDX_D8698A7637FBA45C ON document');
        $this->addSql('ALTER TABLE document CHANGE fk_origin_id origin_id_id INT NOT NULL');
        $this->addSql('ALTER TABLE document ADD CONSTRAINT FK_D8698A76C23E42B3 FOREIGN KEY (origin_id_id) REFERENCES origin (id)');
        $this->addSql('CREATE INDEX IDX_D8698A76C23E42B3 ON document (origin_id_id)');
        $this->addSql('ALTER TABLE email DROP FOREIGN KEY FK_E7927C745741EEB9');
        $this->addSql('ALTER TABLE email DROP FOREIGN KEY FK_E7927C74730AB8CE');
        $this->addSql('DROP INDEX IDX_E7927C745741EEB9 ON email');
        $this->addSql('DROP INDEX IDX_E7927C74730AB8CE ON email');
        $this->addSql('ALTER TABLE email ADD user_id_id INT NOT NULL, ADD email_description_id_id INT NOT NULL, DROP fk_user_id, DROP fk_email_description_id');
        $this->addSql('ALTER TABLE email ADD CONSTRAINT FK_E7927C7430635D72 FOREIGN KEY (email_description_id_id) REFERENCES email_description (id)');
        $this->addSql('ALTER TABLE email ADD CONSTRAINT FK_E7927C749D86650F FOREIGN KEY (user_id_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_E7927C7430635D72 ON email (email_description_id_id)');
        $this->addSql('CREATE INDEX IDX_E7927C749D86650F ON email (user_id_id)');
        $this->addSql('ALTER TABLE phone DROP FOREIGN KEY FK_444F97DD5741EEB9');
        $this->addSql('ALTER TABLE phone DROP FOREIGN KEY FK_444F97DD62082E52');
        $this->addSql('DROP INDEX IDX_444F97DD5741EEB9 ON phone');
        $this->addSql('DROP INDEX IDX_444F97DD62082E52 ON phone');
        $this->addSql('ALTER TABLE phone ADD user_id_id INT NOT NULL, ADD phone_description_id_id INT NOT NULL, DROP fk_user_id, DROP fk_phone_description_id');
        $this->addSql('ALTER TABLE phone ADD CONSTRAINT FK_444F97DD9D86650F FOREIGN KEY (user_id_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE phone ADD CONSTRAINT FK_444F97DDD7F5047 FOREIGN KEY (phone_description_id_id) REFERENCES phone_description (id)');
        $this->addSql('CREATE INDEX IDX_444F97DDD7F5047 ON phone (phone_description_id_id)');
        $this->addSql('CREATE INDEX IDX_444F97DD9D86650F ON phone (user_id_id)');
        $this->addSql('ALTER TABLE upload DROP FOREIGN KEY FK_17BDE61F3D163BCC');
        $this->addSql('ALTER TABLE upload DROP FOREIGN KEY FK_17BDE61FAAC9D403');
        $this->addSql('DROP INDEX IDX_17BDE61F3D163BCC ON upload');
        $this->addSql('DROP INDEX IDX_17BDE61FAAC9D403 ON upload');
        $this->addSql('ALTER TABLE upload ADD affair_id_id INT NOT NULL, ADD document_id_id INT NOT NULL, DROP fk_affair_id, DROP fk_document_id');
        $this->addSql('ALTER TABLE upload ADD CONSTRAINT FK_17BDE61F16E5E825 FOREIGN KEY (document_id_id) REFERENCES document (id)');
        $this->addSql('ALTER TABLE upload ADD CONSTRAINT FK_17BDE61F174FF259 FOREIGN KEY (affair_id_id) REFERENCES affair (id)');
        $this->addSql('CREATE INDEX IDX_17BDE61F16E5E825 ON upload (document_id_id)');
        $this->addSql('CREATE INDEX IDX_17BDE61F174FF259 ON upload (affair_id_id)');
    }
}
