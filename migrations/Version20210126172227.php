<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210126172227 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE affair (id INT AUTO_INCREMENT NOT NULL, user_id_id INT NOT NULL, affairs_number INT NOT NULL, title VARCHAR(50) NOT NULL, description VARCHAR(255) NOT NULL, INDEX IDX_397436879D86650F (user_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE document (id INT AUTO_INCREMENT NOT NULL, origin_id_id INT NOT NULL, label VARCHAR(50) NOT NULL, INDEX IDX_D8698A76C23E42B3 (origin_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE email (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE email_description (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE origin (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE phone (id INT AUTO_INCREMENT NOT NULL, user_id_id INT NOT NULL, phone_description_id_id INT NOT NULL, phone_number INT NOT NULL, INDEX IDX_444F97DD9D86650F (user_id_id), INDEX IDX_444F97DDD7F5047 (phone_description_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE phone_description (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE upload (id INT AUTO_INCREMENT NOT NULL, affair_id_id INT NOT NULL, document_id_id INT NOT NULL, file_path VARCHAR(255) NOT NULL, created_date DATETIME NOT NULL, upload TINYINT(1) NOT NULL, INDEX IDX_17BDE61F174FF259 (affair_id_id), INDEX IDX_17BDE61F16E5E825 (document_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, lastname VARCHAR(50) NOT NULL, firstname VARCHAR(50) NOT NULL, validated_profile TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE affair ADD CONSTRAINT FK_397436879D86650F FOREIGN KEY (user_id_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE document ADD CONSTRAINT FK_D8698A76C23E42B3 FOREIGN KEY (origin_id_id) REFERENCES origin (id)');
        $this->addSql('ALTER TABLE phone ADD CONSTRAINT FK_444F97DD9D86650F FOREIGN KEY (user_id_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE phone ADD CONSTRAINT FK_444F97DDD7F5047 FOREIGN KEY (phone_description_id_id) REFERENCES phone_description (id)');
        $this->addSql('ALTER TABLE upload ADD CONSTRAINT FK_17BDE61F174FF259 FOREIGN KEY (affair_id_id) REFERENCES affair (id)');
        $this->addSql('ALTER TABLE upload ADD CONSTRAINT FK_17BDE61F16E5E825 FOREIGN KEY (document_id_id) REFERENCES document (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE upload DROP FOREIGN KEY FK_17BDE61F174FF259');
        $this->addSql('ALTER TABLE upload DROP FOREIGN KEY FK_17BDE61F16E5E825');
        $this->addSql('ALTER TABLE document DROP FOREIGN KEY FK_D8698A76C23E42B3');
        $this->addSql('ALTER TABLE phone DROP FOREIGN KEY FK_444F97DDD7F5047');
        $this->addSql('ALTER TABLE affair DROP FOREIGN KEY FK_397436879D86650F');
        $this->addSql('ALTER TABLE phone DROP FOREIGN KEY FK_444F97DD9D86650F');
        $this->addSql('DROP TABLE affair');
        $this->addSql('DROP TABLE document');
        $this->addSql('DROP TABLE email');
        $this->addSql('DROP TABLE email_description');
        $this->addSql('DROP TABLE origin');
        $this->addSql('DROP TABLE phone');
        $this->addSql('DROP TABLE phone_description');
        $this->addSql('DROP TABLE upload');
        $this->addSql('DROP TABLE user');
    }
}
