# JLDropOff
As part of the internship completed from 01/11/2021 to 03/05/2021, the JLDropOff application was created. <br/>
Aiming to comply with the General Data Protection Regulation (GDPR), this API was created so that insurance customers can upload the files required for the finalization of their case. <br/>
JLDropOff was made with the PHP Symfony framework, version 5.

## Entities
The JLDropOff project contains the following entities:

    - Affair, which contains the information of each case (case number, title, description),
    as well as the user to which it belongs (fk_user), and its status.

    - Document, which corresponds to the type of document required, the entity has a label,
    a server label (label_server) an origin (fk_origin).
    
    - Email, which contains the email and the user to which it belongs (fk_user),
    as well as its type (fk_emailDescription).

    - EmailDescription, which contains the type (label) of the email.

    - Origin, which contains the origin of the document (label).

    - Phone, which contains the phone number (phone_number) and the user to which it belongs (fk_user),
    as well as its type (fk_phoneDescription).

    - PhoneDescription, which contains the type (label) of phone.

    - Upload, which contains the path to the file (file_path), the creation date (created_date),
    an attribute that allows you to know if the file has been uploaded or not (upload),
    the case to which this file is linked (fk_affair), as well as the type of document it is (fk_document).

    - User, which contains the user's information (email, role, password, name, first name),
    as well as an attribute allowing to know if his account has been validated.

## Controllers
The project contains the following controllers:

    - AffairController, in which are implemented:
        - The create() method, which allows you to create a new business for a customer.
        This method can be used by insurance workers.
        When the case is created, a new case is generated in the server.

        - The getAffair() method, thanks to a request, the user's business (s) are retrieved.
        The method returns a table of cases, containing on the one hand the cases in progress, and on the other hand the completed cases.

    - DocumentController, in which is implemented:
        - The create() method, which allows you to create a new type of document (eg: Driver's license).
        This method can be used by insurance workers.
        At this moment, about twenty documents have been created.

    - EmailController, in which is implemented:
        - The create() method, which allows you to create a new email.
        Then, a getEmail() method, which retrieves the email, and its type (description).

    - EmailDescriptionController, in which is implemented:
        - The create() method, which allows you to create a new type of email (ex: Professional).
        This method can be used by insurance workers.

    - OriginController, in which is implemented:
        - The create() method, which allows you to create a new source for the document.
        This method can be used by insurance workers.
        At this moment, two origins have been created ('JLAssure', which is insurance,
        as well as 'Client', if it is a document requested from the client).

    - PhoneController, in which is implemented:
        - The create() method, which allows you to create a new phone number.
        Then, a getPhone() method, which retrieves the phone number, and its type (description).

    - PhoneDescriptionController, in which is implemented:
        - The create() method, which allows you to create a new type of phone (eg: Professional).
        This method can be used by insurance workers.

    - RegistrationController, in which are implemented:
        - The registration() method, which can be used by insurance employees,
        and which allows you to register a new customer. All clients have a 'ROLE_USER' role.

        - The userRegister() method, which allows the customer, if he has already been registered by the employees,
        to create his account, with a password which will be encoded.
        This method is actually an update of the customer already registered,
        if the user's email is found in the database, it returns a success,
        otherwise, the method returns a refusal.

        - The sendEmail() method, which is called when userRegister () is successful,
        and allows the sending of an automatic email to the user, so that he can validate his account.
        The SwiftMailer bundle is used.
        
        - The activation method(), allowing you to know if the account has been validated.
        Indeed, if the user does not have a token, this means that his account has not been validated.

    - SecurityController, in which is implemented:
        - The login() method, it retrieves the information sent in the form,
        compare them using a database query,
        and therefore to check if the user exists and provided a correct password or not.
        If the information is valid, the method returns success, otherwise it returns an error.

    - UploadController, in which are implemented:
        - The createFile() method, which allows insurance employees to request a new file from the customer.
        In this method, the case for which this file is required, as well as the type of document, are entered.
        A condition is added, if the type of document created is a Contract, and it comes from 'JLAssure',
        then the case is over.

        - The create() method, which allows you to store the file that has been uploaded in the server,
        then store its information (file path, creation date) in the database.
        When an upload is made by a customer, an email is automatically sent to employees
        insurance to inform them.
        This method is discussed step by step in order to understand how it works.

        - The updateFile() method, which allows you to update a file that has been uploaded.
        The file is retrieved by its id, and replaced in the destination.

       - The getMissingFiles() method, which allows you to recover all the missing files of a case,
        thanks to a query which filters the files in which the boolean 'upload' is false.

        - The getUploadedFiles() method, which allows you to retrieve all the files of a case,
        that were uploaded by the client, thanks to a query that filters the files in which
        the boolean 'upload' is true and where the origin of the document is the client.

        - The getReceivedFiles() method, which allows you to retrieve all the files of a case,
        that have been uploaded by insurance, thanks to a query that filters the files in which
        the boolean 'upload' is true and where the origin of the document is 'JLAssure'.

        - The sendEmail() method, which allows the automatic sending of an email when uploading a file,
        for insurance employees. The SwiftMailer bundle is used.

        - The deleteFileFTP() method, which allows the deletion of a file, at the server level,
        as well as the database.

        - The getFile() method, which allows you to retrieve a file using the path to it,
        at the server level.